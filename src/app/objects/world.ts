import { Pallier } from './pallier';
import { Product } from './product';

export class World {
    name: string;
    logo: string;
    money: number;
    score: number;
    totalangels: number;
    activeangels: number;
    angelbonus: number;
    lastupdate: string;
    products: { "product": Product[] };
    allunlocks: { "pallier": Pallier[] };
    upgrades: { "pallier": Pallier[] };
    angelsupgrades: { "pallier": Pallier[] };
    managers: { "pallier": Pallier[] };


    constructor() {
        this.products = { "product": [] };
        this.managers = { "pallier": [] };
        this.upgrades = { "pallier": [] };
        this.angelsupgrades = { "pallier": [] };
        this.allunlocks = { "pallier": [] };
    }
}