import { Injectable } from '@angular/core';
import{ HttpClient, HttpHeaders} from'@angular/common/http';
import { World } from '../objects/world';
import { Pallier } from '../objects/pallier';
import { Product } from '../objects/product';

@Injectable({
  providedIn: 'root'
})
export class RestserviceService {

  server= "http://localhost:8080/";
  user: string;

  constructor(private http: HttpClient) {
    this.user = localStorage.getItem('username');
   }

  privatehandleError(error: any): Promise<any> {
    console.error('An error occurred', error); 
    return Promise.reject(error.message|| error);
  }
  
  getWorld(): Promise<World> {
    console.log(this.user, 'jpppp')
    return this.http.get(this.server + 'adventureisis/generic/world', { headers: this.setHeaders(this.user)})
    .toPromise().catch(this.privatehandleError);
  }

  getServer(){
    return this.server;
  }

  getUser(){
    return this.user;
  }

  setUser(user: string){
    this.user = user;
  }

  private setHeaders(user: string): HttpHeaders {
    let res = new HttpHeaders({'X-User': user});
    return res;
  }
}
