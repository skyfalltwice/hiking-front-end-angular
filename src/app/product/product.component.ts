import { Product } from './../objects/product';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  product: Product;
  server = 'http://localhost:8080/icones/'
  progressbarvalue = 0;
  lastupdate;
  _qtmulti: string;
  _money: number;
  nbproduit = 1;
  price: number;
  revenu: number;
  isbuyable: boolean; 
  producting: boolean = false;

  @Input()
  set prod(value: Product) {
    this.product = value
  }
  @Input()
  set qtmulti(value: string) {
    this._qtmulti = value;
    if (this._qtmulti && this.product) this.calcMaxCanBuy()
  }
  @Input()
  set money(value: number) {
    this._money = value;
  }

  @Output() notifyProduction: EventEmitter<Product> = new
    EventEmitter<Product>();

  @Output() onBuy: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    this.revenu = this.calcRevenu();
    setInterval(() => { this.calcScore(); this.calcMaxCanBuy(), this.checkIfProducting() }, 1)
  }

  startFabrication() {
    if (this.product.quantite > 0) {
      console.log('prout');
      this.product.timeleft = this.product.vitesse;
      this.lastupdate = Date.now();
      this.progressbarvalue = ((this.product.vitesse - this.product.timeleft) / this.product.vitesse) * 100
    }
  }

  calcScore() {
    if (this.product.timeleft > 0) {
      this.product.timeleft = this.product.timeleft - (Date.now() - this.lastupdate);
      this.progressbarvalue = ((this.product.vitesse - this.product.timeleft) / this.product.vitesse) * 100;

      this.lastupdate = Date.now();
      if (this.product.timeleft <= 0) {
        this.product.timeleft = 0;
        this.notifyProduction.emit(this.product);
        this.progressbarvalue = 0;
        if (this.product.managerUnlocked) {
          this.startFabrication();
        }
      }
    }

  }

  calcRevenu() {
    return this.product.quantite * this.product.revenu;
  }

  calcMaxCanBuy() {
    if (this._qtmulti === 'max') {
      const quantity = Math.log(((this.product.croissance * this._money - this._money) / this.product.cout) + 1) / Math.log(this.product.croissance);
      this.nbproduit = +Math.trunc(quantity);
    } else {
      this.nbproduit = parseInt(this._qtmulti, 10);
    }
    this.calcPrice();
    this.isBuyable();
  }

  buyArticle(quantity: number) {
    this._money = this._money - this.calcPrice();
    this.onBuy.emit(this._money);
    this.product.quantite += this.nbproduit;
    this.product.cout = this.product.cout * Math.pow(this.product.croissance, this.nbproduit);
    this.revenu = this.calcRevenu();
  }

  calcPrice() {
    return this.price = this.product.cout * ((1 - Math.pow(this.product.croissance, this.nbproduit))) / (1 - this.product.croissance);
  }

  isBuyable() {
    this.isbuyable = (this.calcPrice() > this._money || this.nbproduit === 0) ? false : true;
  }

  checkIfProducting() {
    if(this.product.managerUnlocked === true && this.producting === false) {
      this.producting = true;
      this.startFabrication();
    }
  }

}
