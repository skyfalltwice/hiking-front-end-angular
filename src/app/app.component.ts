import { Component, OnInit } from '@angular/core';
import { World } from './objects/world';
import { Manager } from './objects/manager';
import { Product } from './objects/product';
import { Pallier } from './objects/pallier';
import { RestserviceService } from './services/restservice.service'
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  //Booleans Pop-up
  openPopUnlocks: boolean = false;
  openPopCash: boolean = false;
  openPopAngels: boolean = false;
  openPopManagers: boolean = false;
  openPopInvestors: boolean = false;

  world: World = new World();
  server: string;

  test: number = 0;

  qtmulti: string = '1';

  managers: Array<Manager> = [
    // { id: 1, image: "./assets/forest-trump.jpg", name: 'Forest Trump', runs: "Orienteur de boussoles", price: 500 },
    // { id: 2, image: "./assets/perry-black.jpg", name: 'Perry Black', runs: "Remplisseur de gourdes", price: 3000 },
    // { id: 3, image: "./assets/richard-ruthless.jpg", name: 'Richard Ruthless', runs: "Réchauffeur de réchaud", price: 6000 },
    // { id: 4, image: "./assets/gordie-palmbay.jpg", name: 'Gordie Palmbay', runs: "Porteur de sacs à dos", price: 10000 },
    // { id: 5, image: "./assets/heisenbird.jpg", name: 'W. Heisenbird', runs: "Confectionneur de duvets", price: 15000 },
    // { id: 6, image: "./assets/jim-thorton.jpg", name: 'Jim Thorton', runs: "Déployeur de tentes", price: 35000 }
  ]

  username: string = 'your name';

  //products = Array<Product>();
  // products = [
  //   { id: 1, name: 'Boussole', image: './assets/boussole.png', quantity: 0, price: 8.70 },
  //   { id: 2, name: 'Gourde', image: './assets/gourde.png', quantity: 0, price: 13.50 },
  //   { id: 3, name: 'Réchaud', image: './assets/rechaud.png', quantity: 0, price: 23.50 },
  //   { id: 4, name: 'Sac à dos', image: './assets/sac-dos.png', quantity: 0, price: 49.99 },
  //   { id: 5, name: 'Duvet', image: './assets/duvet.png', quantity: 0, price: 79.99 },
  //   { id: 6, name: 'Tente', image: './assets/tente.png', quantity: 0, price: 225.50 }
  // ]

  //Managers
  badgeManagers: number = 0;

  constructor(private service: RestserviceService, private snackBar: MatSnackBar) {
    this.server = service.getServer();
    service.getWorld().then(
      world => {
        this.world = world;
      });
  }

  ngOnInit(): void {
    this.username = localStorage.getItem('username');
    setInterval(() => { this.calcManagers() }, 1)
  }

  onUsernameChanged() {
    console.log(this.username);
    localStorage.setItem('username', this.username);
  }

  popMessage(message: string) : void {
    this.snackBar.open(message, '', { duration: 2000});
  }

  onProductionDone(p: Product) {
    this.world.money = this.world.money + (p.revenu * p.quantite);
    // this.world.score = this.world.score + p.revenu;
  }

  onBuyDone(m: number) {
    this.world.money = m;
  }

  changeQtMulti() {
    switch(this.qtmulti) {
      case '1': {
        this.qtmulti = '10'
        break;
      }
      case '10': {
        this.qtmulti = '100'
        break;
      }
      case '100': {
        this.qtmulti = 'max'
        break;
      }
      case 'max': {
        this.qtmulti = '1'
        break;
      }

    }
  }

  hireManager(m: Manager) {
    console.log(this.world.money, m.seuil);
    if(this.world.money > m.seuil) {
      this.world.money = this.world.money - m.seuil;
      m.unlocked = true;
      this.world.products.product[m.idcible-1].managerUnlocked = true;
      this.openPopManagers = false;
      this.popMessage(m.name + ' recruté !');
    }
  }

  calcManagers() {
    // this.badgeManagers = 0;
    // this.world.managers.pallier.forEach(element => {
    //   if(element.seuil < this.world.money) {
    //     this.badgeManagers += 1;
    //   };
    // });
    const res = this.world.managers.pallier.filter(pal => this.filterPalie(pal))
    this.badgeManagers = res.length;
  }

  filterPalie(p: Pallier) {
    if(!p.unlocked && p.seuil < this.world.money) {
      return true
    } else return false;
  }


}
